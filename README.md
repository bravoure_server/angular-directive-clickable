# Bravoure - Angular Directive Clickable

## Use

This Component is used to make blocks entirely clickable.

the class "clickable" should be added to the container
 
### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-directive-clickable": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
