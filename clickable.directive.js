(function () {

    function clickable($location, $window) {
        return {
            restrict: 'C',
            link: function (scope, element, attrs) {

                element.on('click', clickableFunction);

                element.on('click', 'a', clickableLinkFunction);

                function clickableLinkFunction (e) {
                    e.stopPropagation();
                }

                function clickableFunction (e) {

                    var thisLink = $(this).find('a');

                    var target = thisLink.attr('target');
                    var url = thisLink.attr('href');

                    if (target == '_blank') {
                        $window.open(url, target);
                    } else {
                        scope.$apply(function(){
                            $location.path(url);
                        });
                    }

                }
            }
        }
    }

    clickable.$inject = ['$location', '$window'];

    angular
        .module('bravoureAngularApp')
        .directive('clickable', clickable);

})();

